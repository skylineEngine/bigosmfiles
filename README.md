Copyright 2014 Carlos Soriano <carlos.soriano89@gmail.com>


BigOsmFiles
==================================================
Files that due to its large size are better to remain in another repository than
skylineengine


Essential Git
-------------

As the source code is handled by the version control system Git, it's useful to know some features used.

### cleaning ###

If you want to purge your working directory back to the status of upstream, following commands can be used (remember everything you've worked on is gone after these):

```bash
git reset --hard upstream/master
git clean -fdx
```

### handling merge conflicts ###

Since these are files like binaries, better to clean the repository with:
```bash
git reset --hard upstream/master
git clean -fdx
```
And then add the new files to be commited